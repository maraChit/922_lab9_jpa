package ro.ubb.springjpa.service.ServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubb.springjpa.model.Entities.Client;
import ro.ubb.springjpa.model.Exceptions.ValidatorException;
import ro.ubb.springjpa.model.Validators.ClientValidator;
import ro.ubb.springjpa.repository.DBRepository.ClientRepository;
import ro.ubb.springjpa.repository.Sort;
import ro.ubb.springjpa.service.ServiceInterfaces.ClientService;

import javax.transaction.Transactional;
import java.util.List;
@Service
public class ClientServiceImpl implements ClientService {
    public static final Logger log = LoggerFactory.getLogger(ClientServiceImpl.class);

    @Autowired
    private ClientRepository clientRepository;

    @Override
    public void saveClient(Client c) throws ValidatorException {
        ClientValidator.validate(c);
        clientRepository.save(c);
    }

    @Override
    @Transactional
    public void updateClient(Client client) throws ValidatorException {
        log.trace("updateClient - method entered: client={}",client);
        ClientValidator.validate(client);
        clientRepository.findById(client.getId())
                .ifPresent(c->{
                    c.setName(client.getName());
                    log.debug("updateClient - updated: client={}",c);
                });
        log.trace("updateClient- method finished");

    }

    @Override
    public void deleteClient(Long id) {
        clientRepository.deleteById(id);
    }

    @Override
    public List<Client> getAllClients() {
        return clientRepository.findAll();
    }

    @Override
    public Client getClient(Long id) {
        return clientRepository.findById(id).orElse(null);
    }

    @Override
    public List<Client> getSortedClients(Sort sort) {
        return clientRepository.findAll(sort);
    }
}
