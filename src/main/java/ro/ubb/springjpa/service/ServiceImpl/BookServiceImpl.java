package ro.ubb.springjpa.service.ServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubb.springjpa.model.Entities.Book;
import ro.ubb.springjpa.model.Exceptions.ValidatorException;
import ro.ubb.springjpa.model.Validators.BookValidator;
import ro.ubb.springjpa.repository.DBRepository.BookRepository;
import ro.ubb.springjpa.repository.Sort;
import ro.ubb.springjpa.service.ServiceInterfaces.BookService;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class BookServiceImpl implements BookService {
    public static final Logger log = LoggerFactory.getLogger(BookServiceImpl.class);

    @Autowired
    private BookRepository bookRepository;

    @Override
    public void saveBook(Book book) throws ValidatorException {
        BookValidator.validate(book);
        bookRepository.save(book);
    }

    @Override
    @Transactional
    public void updateBook(Book b) throws ValidatorException {
        log.trace("updateBook - method entered: book={}",b);
        BookValidator.validate(b);
        bookRepository.findById(b.getId())
                .ifPresent(book->{
                    book.setAuthor(b.getAuthor());
                    book.setPrice(b.getPrice());
                    book.setTitle(b.getTitle());
                    log.debug("updateBook - updated: book={}",book);
                });
        log.trace("updateBook- method finished");
    }

    @Override
    public void deleteBook(Long id) {
        bookRepository.deleteById(id);
    }

    @Override
    public List<Book> getAllBooks() {
        return bookRepository.findAll();
    }

    @Override
    public Book getBook(Long id) {
        return bookRepository.findById(id).orElse(null);
    }

    @Override
    public List<Book> getSortedBooks(Sort sort) {
        return bookRepository.findAll(sort);
    }
}
