package ro.ubb.springjpa.service.ServiceImpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.ubb.springjpa.model.Entities.Purchase;
import ro.ubb.springjpa.model.Exceptions.ValidatorException;
import ro.ubb.springjpa.model.Validators.PurchaseValidator;
import ro.ubb.springjpa.repository.DBRepository.PurchaseRepository;
import ro.ubb.springjpa.repository.Sort;
import ro.ubb.springjpa.service.ServiceInterfaces.PurchaseService;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PurchaseServiceImpl implements PurchaseService {
    public static final Logger log = LoggerFactory.getLogger(PurchaseServiceImpl.class);

    @Autowired
    private PurchaseRepository purchaseRepository;
    @Override
    public void savePurchase(Purchase p) throws ValidatorException {
        PurchaseValidator.validate(p);
        purchaseRepository.save(p);
    }

    @Override
    @Transactional
    public void updatePurchase(Purchase p) throws ValidatorException {
        log.trace("updatePurchase - method entered: purchase={}",p);
        PurchaseValidator.validate(p);
        purchaseRepository.findById(p.getId())
                .ifPresent(pp->{
                    pp.setDate(p.getDate());
                    pp.setIdBook(p.getIdBook());
                    pp.setIdClient(p.getIdClient());
                    log.debug("updatePurchase - updated: purchase={}",pp);
                });
        log.trace("updatePurchase- method finished");

    }

    @Override
    public void deletePurchase(Long idClient, Long idBook) {
        Long id = findPurchase(idClient,idBook);
        purchaseRepository.deleteById(id);
    }

    @Override
    public List<Purchase> getAllPurchases() {
        return purchaseRepository.findAll();
    }

    @Override
    public Long findPurchase(Long idClient, Long idBook) {
        List<Purchase> listP = new ArrayList<>(purchaseRepository.findAll());
        Optional<Purchase> optional=  listP.stream().filter(x->x.getIdClient().equals(idClient) && x.getIdBook().equals(idBook)).findFirst();
        if (optional.isPresent()) return optional.get().getId();
        return -1L;
    }

    @Override
    public List<Purchase> getSortedPurchases(Sort sort) {
        return purchaseRepository.findAll(sort);
    }
}
