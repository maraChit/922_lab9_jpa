package ro.ubb.springjpa.service.ServiceInterfaces;

import ro.ubb.springjpa.model.Entities.Purchase;
import ro.ubb.springjpa.model.Exceptions.ValidatorException;
import ro.ubb.springjpa.repository.Sort;

import java.util.List;

public interface PurchaseService {
    void savePurchase(Purchase p) throws ValidatorException;
    void updatePurchase(Purchase p) throws ValidatorException;
    void deletePurchase(Long idClient, Long idBook);
    List<Purchase>getAllPurchases();
    Long findPurchase(Long idClient,Long idBook);
    List<Purchase> getSortedPurchases(Sort sort);
}
