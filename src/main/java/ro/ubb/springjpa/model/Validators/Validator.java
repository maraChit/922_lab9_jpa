package ro.ubb.springjpa.model.Validators;

import ro.ubb.springjpa.model.Exceptions.ValidatorException;

public interface Validator<T> {
    void validate(T entity) throws ValidatorException;
}
