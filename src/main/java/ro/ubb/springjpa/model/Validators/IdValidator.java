package ro.ubb.springjpa.model.Validators;

import org.springframework.stereotype.Component;
import ro.ubb.springjpa.model.Exceptions.ValidatorException;
@Component
public class IdValidator  {

    public static void validate(Long entity) throws ValidatorException {
        if(entity == null) throw new ValidatorException("The Id should not be null\n");
        if(entity<=0) throw new ValidatorException("The Id should be greater than 0\n");
    }
}
