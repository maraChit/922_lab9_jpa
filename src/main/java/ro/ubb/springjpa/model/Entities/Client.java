package ro.ubb.springjpa.model.Entities;

import javax.persistence.Entity;

@Entity
public class Client extends BaseEntity<Long> {
    private String name;

    public Client(){}
    public Client(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id="+ getId()+ '\''+
                "name='" + name + '\''+
                '}';
    }
}
